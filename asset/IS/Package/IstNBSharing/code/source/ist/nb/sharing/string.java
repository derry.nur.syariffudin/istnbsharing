package ist.nb.sharing;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.*;
// --- <<IS-END-IMPORTS>> ---

public final class string

{
	// ---( internal utility methods )---

	final static string _instance = new string();

	static string _newInstance() { return new string(); }

	static string _cast(Object o) { return (string)o; }

	// ---( server methods )---




	public static final void removeNewline (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(removeNewline)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required inp_str
		// [o] field:0:required out_str
		IDataCursor cur = pipeline.getCursor();
		String inp_str = IDataUtil.getString(cur,"inp_str");
		String cleanString = inp_str.replaceAll("\r", "").replaceAll("\n", "");
		IDataUtil.put( cur, "out_str", cleanString );
		cur.destroy(); 
			
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	private static int CONCAT_MAX_INPUT_STRINGS_NUMBER=66;
	static String html;
	static int i;
	public static String remove(String html) {
		int a = searchOpenTag(html);
		int b = searchCloseTag(html)+1;
		String html1=html.replace(html.substring(a,b), "");				
		if(html1.indexOf("<")>0) {
			i=0;
			html = remove(html1);
			return html;
		} else {
			return html1;
		}
	}
	public static int searchOpenTag(String html) {	
		while((i<html.length())&&(html.charAt(i)!='<')) {			
			i++;
		}
		return i;
	}
	
	public static int searchCloseTag(String html) {	
		while((html.charAt(i)!='>')&&(i<html.length())) {			
			i++;
		}
		return i;
	}
	// --- <<IS-END-SHARED>> ---
}

