package ist.nb.sharing;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.*;
// --- <<IS-END-IMPORTS>> ---

public final class log

{
	// ---( internal utility methods )---

	final static log _instance = new log();

	static log _newInstance() { return new log(); }

	static log _cast(Object o) { return (log)o; }

	// ---( server methods )---




	public static final void getClientIP (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getClientIP)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required clientIp
		// [o] field:0:required umur
		// [o] field:0:required jmlCalls
		// [o] field:0:required Username
		// pipeline  
		
		Session sessionObject = Service.getSession();
		String clientIp = sessionObject.getName();
		User user = sessionObject.getUser();
		String namauser = user.getName();
		String umur = Long.toString(sessionObject.getAge());
		String jmlcalls = Integer.toString(sessionObject.getCalls());
		
		// pipeline
		IDataCursor pipelineCursor = pipeline.getCursor();
		IDataUtil.put( pipelineCursor, "clientIp", clientIp );
		IDataUtil.put( pipelineCursor, "umur", umur );
		IDataUtil.put( pipelineCursor, "jmlCalls", jmlcalls );
		IDataUtil.put( pipelineCursor, "Username", namauser );
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getElapsedTime (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getElapsedTime)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [i] field:0:required startNanoTime
		// [i] field:0:required endNanoTime
		// [o] field:0:required elapsedTime
		IDataCursor pipelineCursor = pipeline.getCursor(); 
		String startNanoTime = IDataUtil.getString( pipelineCursor, "startNanoTime" );
		String endNanoTime = IDataUtil.getString( pipelineCursor, "endNanoTime" );
		 
		long elapsedTime = (Long.parseLong(endNanoTime) - Long.parseLong(startNanoTime))/1000000;
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "elapsedTime", String.valueOf(elapsedTime));
		pipelineCursor_1.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getNanoTime (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getNanoTime)>> ---
		// @subtype unknown
		// @sigtype java 3.5
		// [o] field:0:required nanoTime
		long nanoTime = System.nanoTime();
		IDataCursor pipelineCursor_1 = pipeline.getCursor();
		IDataUtil.put( pipelineCursor_1, "nanoTime", String.valueOf(nanoTime));
		pipelineCursor_1.destroy(); 
		// --- <<IS-END>> ---

                
	}
}

